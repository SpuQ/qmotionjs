#!/bin/bash

# uninstall.sh
# note: does not uninstall Motion - but restores original operation!

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo -e "\e[91mERROR: The uninstall script must be run as root!" 1>&2
   echo -e "\e[0mExecute the uninstall script in this package as root user with this command:\\n$ sudo bash uninstall.sh"
   exit 1;
fi

# restore default motion.conf file
echo -ne "restoring original motion.conf file... "
if [ -f "/etc/motion/motion.conf.original" ]; then
  mv /etc/motion/motion.conf.original /etc/motion/motion.conf
  echo -e "\e[92mdone\e[39m"
else
  echo -e "\e[91mfailed!\e[39m"
fi

# successful exit!
exit 0;
