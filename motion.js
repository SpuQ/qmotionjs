/**
 *  motion.js
 *  Use 'motion' to detect motion using a camera (e.g. USB webcam). Capture pictures and video's from motion events,
 *  and keep them in an archive.
 **/

const EventEmitter = require('events').EventEmitter;
const fs = require('fs');
const util = require('util');
const path = require('path');

const picturesPath = path.dirname(require.main.filename)+'/public/pictures/';  // path for pictures
const defaultPicture = 'default.jpg';                // default picture
const pictureFile = 'pictureList.json';              // file to save pictures list in
const cameraDevice = '/dev/video0';                  // video device used
const pictureExtension = '.jpg';                     // file extension for picture files
const movieExtension = '.mp4';                       // file extension for movie files
const defaultGalleryMaximum = 10;                    // default maximum amount of gallery elements

util.inherits(Motion, EventEmitter);
module.exports = Motion;

function Motion(){
  var motion;
  var pictureList = [];
  var pictureList_maxLength = defaultGalleryMaximum;
  var pictureUuid;

  // initialize
  init();

  // stop the motion detection software
  this.stopMotionDetection = function(){
    savePictureList();
  	motion.kill('SIGTERM');      // kill 'motion' process
  }
  // start the motion detection software
  this.startMotionDetection = function(){
    var _this = this;
    motion = require('child_process').spawn( 'motion', ['-c',__dirname+'/motion.conf'] );  // spawn Motion with config file!

    motion.stdout.on('data', function(data){
  			data = data.toString();

        // on motion start event
  			if( data.includes("motion start") ){
          pictureUuid = createPicture();
          //console.debug("motion start - new picture created with uuid '"+pictureUuid+"'");
          _this.emit('motion start');
  			}

        // on motion stop event
  			if( data.includes("motion stop") ){
          _this.emit('motion stop');
  			}

  			// on picture save event
  			if( data.includes("picture saved") ){
          var picture = getPictureByUuid( pictureUuid );
  				var oldPath = data.split(/\s+/)[2].trim();  // extract picture path from argument
  				var newPath = 'pictures/'+picture.uuid+pictureExtension;
  				var newAbsolutePath= path.dirname(require.main.filename)+'/public/'+newPath;

          // move the picture to the 'public/pictures/' folder
    			fs.rename(oldPath, newAbsolutePath, function (err) {
            if(!err){
              picture.imagePath = newPath;
              picture.imageAbsolutePath = newAbsolutePath;
              //console.debug(picture.uuid+"'s path set to '"+picture.imageAbsolutePath+"'");
            }
            else{
              //console.debug("err: picture '"+oldPath+"' not copied!");
            }
    		   });
  		  }
        // on movie saved event
        if( data.includes("movie saved") ){
          var picture = getPictureByUuid( pictureUuid );
          var oldPath = data.split(/\s+/)[2].trim();  // extract movie path from argument
          var newPath = 'pictures/'+picture.uuid+movieExtension;
          var newAbsolutePath= path.dirname(require.main.filename)+'/public/'+newPath;
          //console.debug("video file: old path '"+oldPath+"' ");
          // move the movie to the 'public/pictures/' folder
          fs.rename(oldPath, newAbsolutePath, function (err) {
            if(!err){
              picture.moviePath = newPath;
              picture.movieAbsolutePath = newAbsolutePath;
              //console.debug(picture.uuid+"'s path set to '"+picture.movieAbsolutePath+"'");
            }
            else{
              //console.debug("err: movie '"+oldPath+"' not copied!");
            }
           });
        }
        // on motion stop event
        if( data.includes("camera disconnected") ){
          _this.emit('camera disconnected');
        }

  	});
  }

  // check whether camera device is connected
  this.cameraIsConnected = checkForCamera;

  // get picture list
  this.getPictureList = function(){
    return pictureList;
  }

  // set maximum amount of gallery elements
  this.setGalleryMaximum = function( maximum ){
    this.pictureList_maxLength = maximum;
  }

  // delete picture by uuid
  this.deletePictureById = deletePictureById;
  // save picture by uuid
  this.savePictureById = function( uuid ){
    var picture = getPictureByUuid( uuid );
    if(  picture != 'undefined' ){
      picture.saved = true;
      //console.debug("savePicture: '"+uuid+"' saved");
    }
    else{
      //console.debug("savePicture: '"+uuid+"' not found - not saved");
      return -1;
    }
  }
  // unsave picture by uuid
  this.unsavePictureById = function( uuid ){
    var picture = getPictureByUuid( uuid );
    if(  picture != 'undefined' ){
      picture.saved = false;
      //console.debug("savePicture: '"+uuid+"' unsaved");
    }
    else{
      //console.debug("savePicture: '"+uuid+"' not found - not unsaved");
      return -1;
    }
  }
  // cleanup
  this.cleanup = function(){
    //console.debug('cleaning up motion object');
    // save the picture list
    savePictureList();
    //console.debug('done');
  }

  // initialize motion object
  function init(){
    // make sure the required directory is there
    initPicturesDir();
    // get the saved list of pictures
    loadPictureList();
    // clear pictures folder before getting started
    cleanPicturesDirectory();
    // protect the system from excessive pictures
    preserveMemory();
  }

  function initPicturesDir(){
    if( !fs.existsSync( picturesPath ) ){
      //console.debug('Pictures directory does not exist: creating new one\''+picturesPath+'\'');
      // check for 'public' directory - if not there, create it
      var publicDir = path.dirname(require.main.filename)+'/public/';
      if( !fs.existsSync(publicDir) ) fs.mkdirSync(publicDir);
      // check for 'public/pictures' directory - if not there, create it
      if( !fs.existsSync(picturesPath) ) fs.mkdirSync(picturesPath);
    }
    else{
      //console.debug('Pictures directory \''+picturesPath+'\' exists');
    }
    // check for default image - if not there, copy from module directory
    var originalPicturePath = __dirname + '/default.jpg';
    if( !fs.existsSync(picturesPath+'default.jpg') ) fs.copyFileSync(originalPicturePath, picturesPath+'default.jpg');
  }

  // delete picture by uuid
  function deletePictureById( uuid ){
    var picture = getPictureByUuid( uuid );    // find element with UUID in array
    var index;

    if( picture != 'undefined' ){
      // remove actual picture from filesystem
      if(picture.imageAbsolutePath !="") {
        try{
          fs.unlinkSync(picture.imageAbsolutePath);
        } catch(e){}

        try{
          fs.unlinkSync(picture.movieAbsolutePath);
        } catch(e){}
      }
      // remove element from array
      index = pictureList.indexOf( picture );
      if (index > -1) pictureList.splice(index, 1);
      //console.debug("deletePicture: '"+uuid+"' deleted");
      return 0;
    }
    //console.debug("deletePicture: '"+uuid+"' not found - not deleted");
    return -1;
  }

  // prevent going out of memory by limiting the amount of pictures - but preserve saved pictures!
  function preserveMemory(){
    while( pictureList.length > pictureList_maxLength ){
      for( var i=1 ; i<= pictureList.length ; i++){
        if( !pictureList[pictureList.length-i].saved ){
          //console.debug("OOM protection: deleting image '"+pictureList[pictureList.length-i].uuid+"'");
          deletePictureById( pictureList[pictureList.length-i].uuid );        // delete the picture element
          break;                                                              // enough deleted, stop looping
        }
        else {
          //console.debug("OOM protection: image '"+pictureList[pictureList.length-i].uuid+"' is saved, not deleting!");
        }
      }
    }
    //console.debug("OOM protection: picture list length is now '"+pictureList.length+"'");
  }

  // create a picture element in picture list with default settings - returns UUID
  function createPicture(){
    // initialize new picture element
    var picture = {};
    picture.imageDate = new Date();
    picture.imageDateString=picture.imageDate.getDate()+'-'+("0" + (picture.imageDate.getMonth() + 1)).slice(-2)+'-'+picture.imageDate.getFullYear()+'_'+("0"+(picture.imageDate.getHours())).slice(-2)+'u'+("0"+(picture.imageDate.getMinutes())).slice(-2);
    picture.imageDescription=picture.imageDate.getDate()+'/'+("0" + (picture.imageDate.getMonth() + 1)).slice(-2)+'/'+picture.imageDate.getFullYear()+' '+("0"+(picture.imageDate.getHours())).slice(-2)+':'+("0"+(picture.imageDate.getMinutes())).slice(-2);
    picture.imagePath = "pictures/default.jpg";
    picture.imageAbsolutePath = "";
    picture.uuid = generateUuid();
    picture.saved = false;

    // add new picture element to the front of the picture list
    pictureList.unshift( picture );
    // prevent Out Of Memory issues
    preserveMemory();

    //console.debug("new picture created with UUID '"+picture.uuid+"'")
    return picture.uuid;
  }
  // Find picture by UUID
  function getPictureByUuid( uuid ){
    //console.debug("looking for '"+uuid+"'");
    return pictureList.find( picture => picture.uuid === uuid );
  }

  // TODO Load the picture list from the json file
  function loadPictureList(){
    try{
      pictureList = require(picturesPath+'pictureList.json');
    } catch (e){
      //console.debug("couldn't load picture list:\n"+e);
      return -1;
    }
    //console.debug("picture list loaded");
    return 0;
  }
  // TODO Save the picture list to the json file
  function savePictureList(){
    let data = JSON.stringify(pictureList);

    fs.writeFileSync(picturesPath+'pictureList.json', data, (err) => {
        if (err) throw err;
        //console.debug('Data written to file');
    });
  }

  // deletes all content of the 'pictures' directory, except the default picture
  function cleanPicturesDirectory(){
    fs.readdir(picturesPath, function(err, files) {                     // list all files in Pictures directory
      files.forEach( function( filename ){                              // loop over the list of files
        if( filename != defaultPicture && filename != pictureFile ){    // preserve default picture and the saved picture list
          var foundFlag = false;
          for (i in pictureList) {
            if ( pictureList[i].uuid+pictureExtension == filename || pictureList[i].uuid+movieExtension == filename ) {    // check if picture or movie file should exist
              foundFlag = true;
              break;
            }
          }
          if( !foundFlag ){
            //console.debug("deleting '"+filename+"'");
            fs.unlinkSync(picturesPath+filename);
          }
        }
      });
    });
  }
  // generate a uuid
  function generateUuid(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  // check whether the camera is present
  function checkForCamera(){
    if( fs.existsSync(cameraDevice) ){
      //console.debug("camera '"+cameraDevice+"' is connected");
      return 1;
    }
    else{
      //console.debug("camera '"+cameraDevice+"' is not connected");
      return 0;
    }
  }
}
