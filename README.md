# QmotionJS
Use '[motion](https://motion-project.github.io/ "motion")' to detect motion using a camera (e.g. USB webcam). Capture pictures and video's from motion events, and keep them in an archive.

:warning: **Linux-based systems only! (e.g. your Raspberry Pi, or your Ubuntu workstation)**

## Basic Usage
Start the motion detection software on the default camera (/dev/video0).
```
.startMotionDetection()
```
Get the list of motion detection event objects. This function returns an array of Motion Event Objects
```
.getPictureList()
```
Motion event object:
```
{
  imagePath: string - /relative/path/to/image/file/
  imageAbsolutePath: string - /absolute/path/to/image/file/
  moviePath: string - /relative/path/to/movie/file/
  movieAbsolutePath: string - /absolute/path/to/movie/file/
  imageDate: date object - moment of creation
  imageDateString: string of date
  imageDescription: some description
  uuid: string - unique ID
  saved: true/false - if 'true' the object will not be deleted for memory saving
}
```
Operations that can be preformed on motion event objects:
```
.deletePictureById( id ) - deletes the Motion Event Object with given ID
.savePictureById( id ) - sets the 'saved' field to 'true'; object must be kept
.unsavePictureById( id ) - sets the 'saved' field to 'false': object may be deleted
```

Events emitted by this object:
```
motion start - emitted at the start of detected motion
motion stop - emitted at the end of detected motion
camera disconnected - emitted when the camera is disconnected
```

### TODO's
This works, but some more thought and cleanup should be put into this!
- cleanup naming of functions/variables
- camera reconnection event
- ...


## Collaborate
Don't hesitate to contact the project maintainer(s)!
