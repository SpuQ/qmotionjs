#!/bin/bash

# postinstall.sh
# script for installing Linux userspace driver for Qcom
# from GitLab repository.

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo -e "\e[91mERROR: The postinstall script must be run as root!" 1>&2
   echo -e "\e[0mExecute the postinstall script in this package as root user with this command:\\n$ sudo bash postinstall.sh"
   exit 1;
fi

# Check for Motion, download and install if not present
echo -ne "Checking for Motion... "
if [ -x "$(command -v motion)" ]; then
	echo -e "\e[92m installed \e[39m"
else
	echo -e "\e[91m not installed \e[39m"
	echo -ne "Installing Motion... "
  apt-get update -qq -y
  apt-get install motion -qq -y
	echo -e "\e[92mdone\e[39m"
fi

# replace default config file with motion.conf
# if the original motion.conf file already has been saved, don't overwrite!
if ! [ -f "/etc/motion/motion.conf.original" ]; then
  echo -ne "saving original motion.conf file... "
  mv /etc/motion/motion.conf /etc/motion/motion.conf.original
  echo -e "\e[92mdone\e[39m"
fi
echo -ne "installing new motion.conf file... "
cp ./motion.conf /etc/motion/motion.conf
echo -e "\e[92mdone\e[39m"

# TODO: Create directory for the pictures in project folder (not tested!)
#if ![ -d "./public/" ] then; mkdir ./public; fi;
#if ![ -d "./public/pictures/" ] then; mkdir ./public/pictures; fi;
#if ![ -f "./public/pictures/default.jpg" ] then; mv ./default.jpg ./public/pictures/default.jpg; fi;


# successful exit!
exit 0;
